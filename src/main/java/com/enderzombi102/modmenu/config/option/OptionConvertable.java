package com.enderzombi102.modmenu.config.option;

public interface OptionConvertable {
	Option asOption();
}
