package com.enderzombi102.modmenu.gui.widget;

public interface Renderable {
	void render( int mouseX, int mouseY, float delta );
}
