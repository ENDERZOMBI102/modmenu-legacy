package com.enderzombi102.modmenu.gui.widget.entries;

import com.enderzombi102.modmenu.api.Mod;
import com.enderzombi102.modmenu.gui.widget.ModListWidget;

public class IndependentEntry extends ModListEntry {
	public IndependentEntry( Mod mod, ModListWidget list ) {
		super( mod, list );
	}
}
